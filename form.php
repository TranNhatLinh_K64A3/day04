<?php
$sex = array(
    "Nam",
    "Nữ"
);

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);




?>

<?php
$error = "";

function validateDate($date, $format = 'd/m/Y')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!isset($_POST['hoten']) || $_POST['hoten'] == null) {
        $error .= "Hãy nhập tên.<br>";
    }

    if (!isset($_POST['sex']) || $_POST['sex'] == null) {
        $error .= "Hãy chọn giới tính.<br>";
    }

    if (!isset($_POST['department']) || $_POST['department'] == null) {
        $error .= "Hãy chọn phân khoa.<br>";
    }

    if (!isset($_POST['birthday']) || $_POST['birthday'] == null) {
        $error .= "Hãy nhập ngày sinh.<br>";
    } else if (!validateDate($_POST['birthday'])) {
        $error .= "Hãy nhập ngày sinh đúng định dạng.";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="theme.css">
    <title>Document</title>
</head>

<body>

    <script type="text/javascript">
        $(function() {
            $('#txt_date').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>


    <div class="wrapper">
        <form method="POST">
            <div class="error">
                <?php
                echo $error
                ?>
            </div>

            <div class="name_input">
                <label class="red_star">Họ và tên</label>
                <input class="input_name" type="text" name="hoten">
            </div>

            <div class="sex_select">
                <label class="red_star">Giới tính</label>
                <?php
                for ($i = 0; $i < count($sex); $i++) {
                    echo "
                            <input class=\"input_sex\" name = \"sex\" type=\"radio\">{$sex[$i]}
                        ";
                }
                ?>
            </div>

            <div class="department">
                <label class="red_star">Phân khoa</label>
                
                <select class="custom" name="department">
                    <option value=""></option>
                    <?php
                    foreach (array_keys($department) as $dep) {
                        echo '
                                <option value="' . $dep . '">' . $department[$dep] . '</option>
                            ';
                    }
                    ?>
                </select>
            
            </div>

            <div class="date_input">
                <label class="red_star">Ngày sinh</label>
                <input id="txt_date" type="text" class="input_date" name="birthday" placeholder="dd/mm/yyyy" />
            </div>

            <div class="address_input">
                <label>Địa chỉ</label>
                <input class="input_name" type="text">
            </div>

            <button type="submit">Đăng ký</button>
        </form>
    </div>

</body>

</html>